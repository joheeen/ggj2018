﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController singleton;

	[SerializeField] List<Transform> _babyDolfins;
	[SerializeField] TimerScript _timer;
	[SerializeField] GameObject _endScreen;


	void Start()
	{
		if (singleton == null)
			singleton = this;

		if (singleton != this)
			Destroy(this);

		DontDestroyOnLoad(this);
	}

	
	public void AddTime(float time)
	{
		_timer.AddTime(time);
	}

	public void CaughtDolphin(Transform baby)
	{
        if (_babyDolfins.Contains(baby))
            return;
        
		_babyDolfins.Remove(baby);
		Destroy(baby.gameObject);

		if (_babyDolfins.Count == 0)
			EndScreen("Well done you caught all the babies!");
	}

	public void EndScreen(string text)
	{
		_endScreen.GetComponentInChildren<Text>().text = text;
		_endScreen.SetActive(true);
	}
}
