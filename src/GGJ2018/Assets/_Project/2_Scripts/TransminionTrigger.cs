﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Tigger logic for the transminion is set here
public partial class Transminion : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Interactable")
        {
            //Check which interactable type
            IInteractable interactable = col.gameObject.GetComponent<IInteractable>();
            if (interactable != null)
            {
                interactable.Interact();
            }

            IInteractableWithAmount interactableWithAmount = col.gameObject.GetComponent<IInteractableWithAmount>();
            if (interactableWithAmount != null)
            {
                this._interactableWithAmount = interactableWithAmount;
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Interactable")
        {
            if (col.gameObject.GetComponent<IInteractableWithAmount>() == _interactableWithAmount)
                InvokeOnTriggerExit();
        }
    }
}
