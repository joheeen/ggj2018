﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DolphinNose : MonoBehaviour {

	[SerializeField] float _extraTimeSonarRing;
	[SerializeField] float _extraTimeCaughtBaby;

	void OnTriggerEnter(Collider other)
	{
		SonarWave sonar = other.GetComponent<SonarWave>();
		if (sonar != null && !sonar.Collided && sonar.IsForParent)
			GameController.singleton.AddTime(_extraTimeSonarRing);

		if(other.GetComponent<BabyDolphin>())
		{
			GameController.singleton.CaughtDolphin(other.transform);
			GameController.singleton.AddTime(_extraTimeCaughtBaby);
		}

	}
}
