﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Reference/Players")]
public class PlayersReference : ScriptableObject
{
	public Transform players;
}
