﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeStampBehaviour : MonoBehaviour {

	Text _text;
	Color _color;
	[SerializeField] float _fallSpeed;

	// Use this for initialization
	void Start () {
		_text = GetComponent<Text>();
		_color = _text.color;
	}
	
	// Update is called once per frame
	void Update () {

		_color.a -= Time.deltaTime;
		_text.color = _color;
		transform.position -= new Vector3(0, _fallSpeed, 0);

		if (_color.a <= 0)
			Destroy(gameObject);

	}
}
