﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoplhinControlSonar : DolphinControl, IInteractable {

    [SerializeField] Sonar _sonar;

    public void Interact()
    {
		_sonar.Activated();
    }
}
