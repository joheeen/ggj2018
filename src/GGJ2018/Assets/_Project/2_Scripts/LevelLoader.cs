﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

    #if !UNITY_EDITOR
	// Use this for initialization
	void Awake () {
        SceneManager.LoadScene("scene_level", LoadSceneMode.Additive);
	}
    #endif

}
