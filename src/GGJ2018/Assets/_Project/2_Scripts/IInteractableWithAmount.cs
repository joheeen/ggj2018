﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractableWithAmount {

    void Interact(float amount);
}
