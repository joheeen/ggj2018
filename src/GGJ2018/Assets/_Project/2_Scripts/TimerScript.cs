﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

	[SerializeField] Image _timeBar;
	[SerializeField] float _time;
	[SerializeField] GameObject _timeStamp;
	[SerializeField] Transform _spawnPos;
	float devider;

	// Use this for initialization
	void Start () {
		devider = _time;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTimeBar();
	}

	void UpdateTimeBar()
	{
		_time -= Time.deltaTime;
		_timeBar.fillAmount = (_time / devider);

		if(_time <= 0)
		{
			GameController.singleton.EndScreen("That's unfortunate, \nyou're out of time");
			enabled = false;
		}
	}

	public void AddTime(float time)
	{
		_time += time;

		GameObject stamp = Instantiate(_timeStamp, transform);
		stamp.GetComponent<Text>().text = "+" + time;
		stamp.transform.position = _spawnPos.position;
	}
}
