﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DolphinControlVertical : DolphinControl, IInteractableWithAmount 
{
    [SerializeField] Transform _rotateOnInteraction;
    [SerializeField] float _rotationOnInteractSpeed = 10f;

    public void Interact(float amount) 
    {
        _dolphin.transform.Rotate(Vector3.left * Time.deltaTime * amount * _dolphin.rotationSpeed);
        _rotateOnInteraction.Rotate(Vector3.forward * Time.deltaTime * amount * _rotationOnInteractSpeed);
    }
}
