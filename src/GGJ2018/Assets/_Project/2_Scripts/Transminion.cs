﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;


public partial class Transminion : MonoBehaviour
{
    public XboxController controller;
    public Material mat;

    //Set the element with which the player can interact
    IInteractableWithAmount _interactableWithAmount;

    [SerializeField] float _speed;
    public float MinimumValue = 0.1f;
    float _defaultSpeed;

    // Use this for initialization
    void Start()
    {
        _defaultSpeed = _speed;

        XCI.DEBUG_LogControllerNames();
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        RotatePlayer();

        //Interact with an object by using the right trigger
        if (_interactableWithAmount != null)
        {
            _interactableWithAmount.Interact(RightJoystickRotationDifference());
        }
    }

    void MovePlayer()
    {
        //Run
        if (XCI.GetButton(XboxButton.RightBumper, controller))
        {
            _speed = _defaultSpeed * 2;
        }
        else
        {
            _speed = _defaultSpeed;
        }

        //Walk
        float h = XCI.GetAxis(XboxAxis.LeftStickX, controller);
        float v = XCI.GetAxis(XboxAxis.LeftStickY, controller);

        if (Mathf.Abs(h) > MinimumValue)
            h *= _speed * Time.deltaTime;
        else
            h = 0;

        if (Mathf.Abs(v) > MinimumValue)
            v *= _speed * Time.deltaTime;
        else
            v = 0;
        transform.localPosition += new Vector3(h, 0, v);
    }

    void RotatePlayer()
    {
        float x = XCI.GetAxis(XboxAxis.LeftStickX, controller);
        float y = XCI.GetAxis(XboxAxis.LeftStickY, controller);

        //Calculate difference between this and previous frame
        float rotation = ( (x > 0) ? Mathf.Rad2Deg * (Mathf.Atan2(x, y)): (Mathf.Rad2Deg * Mathf.Atan2(Mathf.Abs(x), -y)) + 180);

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, rotation, transform.eulerAngles.z);
    }
        
    float differenceRotation = 0;
    float totalRotation = 0;
    float prevRotation = 0;
    [Range(0f, 1f)] public float joyStickThreshold = 0.3f;
    float a =0;
    float prev = 0;
    bool _first;

    float RightJoystickRotationDifference()
    {
        float x = XCI.GetAxis(XboxAxis.RightStickX, controller);
        float y = XCI.GetAxis(XboxAxis.RightStickY, controller);

        //Threshold
        if (Mathf.Abs(x) < joyStickThreshold && Mathf.Abs(y) < joyStickThreshold)
        {
            _first = true;
            prev = 0f;
            return 0.0f;
        }
            
        //Calculate difference between this and previous frame
        float cur = ( (x > 0) ? Mathf.Rad2Deg * (Mathf.Atan2(x, y)): (Mathf.Rad2Deg * Mathf.Atan2(Mathf.Abs(x), -y)) + 180);

        if (_first)
        {
            prev = cur;
            _first = false;
        }

        float d = Mathf.Abs(cur - prev) % 360; 
        int sign = (cur - prev >= 0 && cur - prev <= 180) || (cur - prev <=-180 && cur- prev>= -360) ? 1 : -1; 
        float r = d > 180 ? 360 - d : d;
        r *= sign;

        //Set prev
        prev = cur;

        //Return rotation diff
        return r;
    }

    //Reset values
    void InvokeOnTriggerExit()
    {
        _interactableWithAmount = null;
    }
}
