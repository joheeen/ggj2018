﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DolphinControlForward : DolphinControl, IInteractable 
{
    bool _active;
    [Header("Optional")]
    public Animation animationOnTrigger;

    void Awake()
    {
        _active = false;
    }

    public void Interact() 
    {   
        _active = !_active;

        if (_active && animationOnTrigger != null)
            animationOnTrigger.Play();
    }

    void Update()
    {
        if (_active)
        {
            _dolphin.transform.Translate(Vector3.forward * Time.deltaTime * _dolphin.speed);
        }
    }
}
