﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarReceiver : MonoBehaviour {

	protected virtual void OnTriggerEnter(Collider other)
	{
		SonarWave sonar = other.GetComponent<SonarWave>();
		if (sonar != null)
			DoAction(sonar);
	}

	protected virtual void DoAction(SonarWave sonar)
	{
	}
}
