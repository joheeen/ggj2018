﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpawnSonarWaves))]
public class ChildDolphinReceiver : SonarReceiver {

	[SerializeField] PlayersReference _players;
	SpawnSonarWaves _waveSpawner;

	void Start()
	{
		_waveSpawner = GetComponent<SpawnSonarWaves>();
	}

	protected override void DoAction(SonarWave sonar)
	{
		if (sonar.IsForParent)
			return;

		StartCoroutine(sonar.FadeAway(0));

		if (sonar.ShowDot)
			_waveSpawner.Activated(transform, _players.players);
	}
}
