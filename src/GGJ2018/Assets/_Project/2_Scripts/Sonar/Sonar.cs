﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar : MonoBehaviour {

	[Header("Sonar UI Information")]
	[SerializeField] SonarCanvas _sonarCanvas;
	[SerializeField] SpawnSonarWaves[] _spawners;

	public void Activated()
	{
		foreach (var spawner in _spawners)
		{
			spawner.Activated(null, null);
		}
	}

	/// <summary>
	/// Show the sonar visuals
	/// </summary>
	public void ShowSonarVisuals(Transform origin)
	{
		_sonarCanvas.ShowSonarData(origin, transform);
	}
}
