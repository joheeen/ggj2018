﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarCanvas : MonoBehaviour {

	[SerializeField] Transform _sonarDot;
	[SerializeField] Transform _heightDot;
	[SerializeField] float _scaler;

	const string _animTrigger = "On";
	Animator _sonarAnim;

	Transform _baby;
	Transform _players;

	void Start()
	{
		_sonarAnim = GetComponentInChildren<Animator>();
	}

	public void ShowSonarData(Transform baby, Transform players)
	{
		_baby = baby;
		_players = players;

		SetSonarPosition();
		SetHeightPosition();
		_sonarAnim.SetTrigger(_animTrigger);

	}

	void SetSonarPosition()
	{
		//Get the position offset
		Vector2 pos = GetBabyPosition();

		pos.x *= _scaler;
		pos.y *= _scaler;

		//set objects position and active it
		_sonarDot.localPosition = pos;
	}

	void SetHeightPosition()
	{
		//Get height and create vector2
		float height = GetBabyHeight();
		Vector2 h = new Vector2(0, height);

		//Set objects height and activate
		_heightDot.localPosition = h;
	}

	/// <summary>
	/// Get the position of the baby from the players perspective
	/// </summary>
	/// <returns></returns>
	Vector2 GetBabyPosition()
	{
		if (_baby == null)
			return Vector2.zero;

		Vector2 pos = new Vector2(_players.position.x, _players.position.z);
		Vector2 babyPos = new Vector2(_baby.position.x, _baby.position.z);

		return babyPos - pos;
	}

	/// <summary>
	/// Get the height of the baby
	/// </summary>
	/// <returns></returns>
	float GetBabyHeight()
	{
		if (_baby == null)
			return 0;

		return _baby.position.y - _players.position.y;
	}

}
