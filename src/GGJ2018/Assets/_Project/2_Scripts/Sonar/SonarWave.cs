﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarWave : MonoBehaviour
{
	[SerializeField] float _waveSpeed;
	[SerializeField] float _growthSpeed;
	[SerializeField] float _maxScale;
	[SerializeField] float _lifeTime;
	Vector3 direction;
	int dir = 1;
	bool _moveWaves;
	public bool ShowDot;
	public bool Collided;
	public bool IsForParent;
	public Transform origin;

	Material _material;
	Coroutine _coroutine;

	void Start()
	{
		_material = GetComponent<MeshRenderer>().material;
		_coroutine = StartCoroutine(FadeAway(_lifeTime));
	}

	// Update is called once per frame
	void Update()
	{
		if (_moveWaves)
		{
			transform.position += (transform.forward * dir) * _waveSpeed * Time.deltaTime;

			//if not bigger than the max scale, scale up
			if (transform.localScale.x < _maxScale)
				transform.localScale *= _growthSpeed;
		}
	}

	/// <summary>
	/// For the child dolphin
	/// </summary>
	/// <param name="baby"></param>
	/// <param name="player"></param>
	public void MoveToPlayers(Transform baby, Transform player)
	{
		origin = baby;
		direction = baby.position - player.position;
		transform.forward = direction;
		dir = -1;
		_moveWaves = true;
	}

	/// <summary>
	/// For the parent dolphin
	/// </summary>
	/// <param name="player"></param>
	public void MoveToPlayers(Transform player)
	{
		transform.forward = player.forward;
		dir = 1;
		_moveWaves = true;
	}

	public IEnumerator FadeAway(float delay)
	{
		yield return new WaitForSeconds(delay);

		if (delay == 0)
			StopCoroutine(_coroutine);

		float i = 1;
		Color c = _material.color;

		while (i > 0)
		{
			i -= Time.deltaTime;
			c.a = i;
			_material.color = c;

			yield return null;
		}

		Destroy(gameObject);
	}
}
