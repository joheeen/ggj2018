﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentDolphinReceiver : SonarReceiver {

	Sonar _sonar;

	void Start()
	{
		_sonar = GetComponentInParent<Sonar>();
	}

	protected override void DoAction(SonarWave sonar)
	{
		if (!sonar.IsForParent)
			return;

		StartCoroutine(sonar.FadeAway(0));

		sonar.Collided = true;

		if (sonar.ShowDot)
			_sonar.ShowSonarVisuals(sonar.origin);
	}
}
