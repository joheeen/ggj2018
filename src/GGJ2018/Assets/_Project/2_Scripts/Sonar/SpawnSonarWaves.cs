﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSonarWaves : MonoBehaviour {

	public GameObject SonarWave;
	public int RingAmounts;
	public float TimeInBetween;

	public void Activated(Transform baby, Transform players)
	{
		StartCoroutine(SpawnWaves(baby, players));
	}

	IEnumerator SpawnWaves(Transform baby, Transform players)
	{
		for (int i = 0; i < RingAmounts; i++)
		{
			GameObject wave = (GameObject)Instantiate(SonarWave, transform.position, Quaternion.Euler(transform.forward));
			SonarWave sonar = wave.GetComponent<SonarWave>();

			if (baby == null)
				sonar.MoveToPlayers(transform);
			else
				sonar.MoveToPlayers(baby, players);

			if(baby != null && i == 0)
				sonar.ShowDot = true;
			else if (i == (int)RingAmounts / 2)
				sonar.ShowDot = true;

			yield return new WaitForSeconds(TimeInBetween);
		}
	}
}
