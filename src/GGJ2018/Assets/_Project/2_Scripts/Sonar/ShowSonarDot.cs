﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSonarDot : MonoBehaviour {

	[SerializeField] float _turnOffDelay;
	[SerializeField] Color _dotsColor;

	[SerializeField] Collider2D _stripe;
	[SerializeField] RawImage _heightDot;
	RawImage _image;

	void Start()
	{
		_image = GetComponent<RawImage>();
		_dotsColor.a = 0;
		_image.color = _dotsColor;
		_heightDot.color = _dotsColor;
	}


	void OnTriggerEnter2D(Collider2D collision)
	{
		if (ReferenceEquals(_stripe, collision))
			StartCoroutine(ShowDots());
	}

	IEnumerator ShowDots()
	{
		_dotsColor.a = 1;
		_image.color = _dotsColor;

		while(_dotsColor.a > 0)
		{
			_dotsColor.a -= Time.deltaTime;
			_image.color = _dotsColor;
			_heightDot.color = _dotsColor;
			yield return null;
		}
	}
}
