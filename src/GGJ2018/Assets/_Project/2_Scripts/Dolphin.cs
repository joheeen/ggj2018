﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dolphin : MonoBehaviour {

    [Header("Settings")]
	public float rotationSpeed;
    public float speed;

    [Header("Miscelanious")]
    [SerializeField] Sonar _sonar;
	[SerializeField] PlayersReference _reference;

	// Use this for initialization
	void Start () {
		_reference.players = transform;
	}
	

	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
            //debugInteractible.Interact();
            _sonar.Activated();
    }
}
